pub struct Wave {
    pub velocity: f32, 
    pub lambda: f32, 
    pub frecuece: f32 , 
    pub cosin_frecuence: f32,
    pub period: f32 ,
    pub emisor_vel: f32,
}

impl Wave {
    pub fn new() -> Wave {
        Wave {
            velocity : 0f32, lambda : 0f32, frecuece: 0f32 ,
            period: 0f32, emisor_vel: 0f32, cosin_frecuence: 0f32, }
        }
    

    pub fn get_f_vl_iv(&mut self) -> f32 {

        self.frecuece =  self.velocity/self.lambda;
        self.velocity / self.lambda
    }

    pub fn get_f_vl_ev(&mut self, v: f32, l: f32) -> f32 {

        self.frecuece =  v/l;
        v/l
    }

    
    pub fn get_p_1f(&mut self,) -> f32 {
        
        self.period = (1f32/self.frecuece) as f32;
        (1f32/self.frecuece) as f32
    }


    pub fn get_l_from_cm(&mut self, i: f32) -> f32 {

        self.lambda = i/100f32;
        i/100f32
    }


    pub fn copare_bt_wv_ev_with_t(&self, t: f32) -> f32 {

        self.velocity * (t as f32)
    }

    pub fn doppler_effect_sound_iv(&mut self, s: i8, st: i8) -> f32 {
        if s > 0 {
            self.cosin_frecuence = (self.velocity / (self.velocity + self.emisor_vel )) * self.frecuece;
            self.cosin_frecuence
        } else { 
            self.cosin_frecuence = (self.velocity / (self.velocity - self.emisor_vel )) * self.frecuece;
            self.cosin_frecuence
        }
    }
    pub fn doppler_effect_sound_ev(&mut self, s: i8, v: f32, ev: f32, f: f32) -> f32 {
        self.velocity = v;
        self.emisor_vel = ev;
        self.frecuece = f ;
        if s > 0 {
            self.cosin_frecuence = (self.velocity / (self.velocity + self.emisor_vel )) * self.frecuece;
            self.cosin_frecuence
        } else { 
            self.cosin_frecuence = (self.velocity / (self.velocity - self.emisor_vel )) * self.frecuece;
            self.cosin_frecuence
        }
    }
}

//---------------------------------------------

pub struct SoundWave {
    pub extend: Wave,
    //velocities
    pub velocity_with_temp:f32,
    //temp
    pub temp_cg: f32,
}

pub const soundwave_velocity_std: f32 = 343.2;
pub const soundwave_temp_cg_std: f32 = 20.333;
pub const soundwave_velocity_std_symp: f32 = 340f32;

impl SoundWave {
    pub fn new() -> SoundWave {
            SoundWave{
            extend: Wave::new() , velocity_with_temp: 0f32, temp_cg: 0f32 }
        }


    pub fn get_v_with_temp(&mut self, t: f32){
        self.velocity_with_temp = 331f32 + (0.6f32 * t)
    }


    pub fn get_t_with_vel(&mut self){
        self.temp_cg = (self.extend.velocity - 331f32)/0.6f32;
    }


    pub fn get_t_with_vel_u(&mut self, v: f32){
        self.extend.velocity = v as f32; 
        self.temp_cg = (self.extend.velocity - 331f32)/0.6f32;
    }
}