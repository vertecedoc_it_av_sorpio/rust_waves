mod waves;
use waves::soundwave_velocity_std_symp as awsts;

fn main() {

let mut onda_pregunta_1 = waves::Wave::new();

onda_pregunta_1.velocity = 140f32;

onda_pregunta_1.get_l_from_cm(35f32);

onda_pregunta_1.get_f_vl_iv();

println!("{}hz",onda_pregunta_1.frecuece);

let mut sonido_pregunta_2 = waves::SoundWave::new();

sonido_pregunta_2.extend.velocity = awsts;

let sc1 = sonido_pregunta_2.extend.copare_bt_wv_ev_with_t(5f32);
let sc2 = sonido_pregunta_2.extend.copare_bt_wv_ev_with_t(5f32)/(1000f32);

println!("{} metros, {} km",sc1, sc2);

let mut sonido_pregunta_3 = waves::SoundWave::new();
sonido_pregunta_3.extend.doppler_effect_sound_ev(-1, awsts, 20f32, 640f32); 

println!("el effecto doppler es: {}" , sonido_pregunta_3.extend.cosin_frecuence);

}